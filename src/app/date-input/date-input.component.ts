
import { Component } from '@angular/core';

@Component({
  selector: 'app-date-input',
  templateUrl: './date-input.component.html',
  styleUrls: ['./date-input.component.css']
})
export class DateInputComponent {

  days:number[]=[];
  months:string[]= ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
  years:number[]=[];

  getDays(){
    var i=1;
   for(;i<=31;i++){
     this.days.push(i);
   }
   return this.days;
  };
  getYears(){
    var i=1980;
    for(;i<=2011;i++){
      this.years.push(i);
    }
    return this.years;
  };
}
