import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenderCheckComponent } from './gender-check.component';

describe('GenderCheckComponent', () => {
  let component: GenderCheckComponent;
  let fixture: ComponentFixture<GenderCheckComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenderCheckComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenderCheckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
